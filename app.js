const metin = document.querySelector(".degisen-yazi");

const dizi = ["HIZLI MÜDAHALE", "TAM ZAMANLI KONTROL"];
let diziIndex = 0;
let charIndex = 0;

function type(){
    if(charIndex < dizi[diziIndex].length){
        metin.textContent += dizi[diziIndex].charAt(charIndex);
        charIndex++;
        setTimeout(type, 200);
    }else
    {
        setTimeout(erase, 2000);            
    }
}

function erase(){
    charIndex = 0;
    diziIndex++;
    metin.innerHTML = ''

    if(diziIndex >= dizi.length){
        diziIndex = 0;
    }

    setTimeout(type, 0);
}

document.addEventListener("DOMContentLoaded", () => {
    setTimeout(type, 200);
});